<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AlterUsers extends Migration
{
    public function up()
    {
        //
        $this->forge->addColumn('users', [
            'avatar VARCHAR(255) '
        ]);
    }

    public function down()
    {
        //
        $this->forge->dropColumn('users', 'avatar');
    }
}
