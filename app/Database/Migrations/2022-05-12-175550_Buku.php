<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Buku extends Migration
{
    public function up()
    {
        //
        $this->forge->addField([
            'id'               => ['type' => 'int', 'constraint' => 11, 'unsigned' => true, 'auto_increment' => true],
            'judul_buku'            => ['type' => 'varchar', 'constraint' => 255],
            'pengarang'            => ['type' => 'varchar', 'constraint' => 255],
            'tahun_terbit'         => ['type' => 'int', 'constraint' => 5, 'null' => true],
            'created_at'       => ['type' => 'datetime', 'null' => true],
            'updated_at'       => ['type' => 'datetime', 'null' => true],
            'deleted_at'       => ['type' => 'datetime', 'null' => true],
        ]);

        $this->forge->addKey('id', true);

        $this->forge->createTable('buku', true);
    }

    public function down()
    {
        //
        $this->forge->dropTable('buku', true);
    }
}
