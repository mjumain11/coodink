<?= $this->extend('template/template') ?>
<?= $this->section('main') ?>
<?php
    $encryption = new \Daycry\Encryption\Encryption();
?>
<div class="col col-md-12">
    <?php $session = \Config\Services::session();; if (!empty($session->getFlashdata('message'))) : ?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <?= $session->getFlashdata("message").'</br>' ?>
        </div>
    <?php endif; ?>
</div>
<div class="card border-success mb-3">
  <div class="card-header">
    <a href="<?= base_url('add_buku') ?>"><button type="button" class="btn btn-primary btn-sm">Tambah Data</button></a>
  </div>
  <div class="card-body">
    <table class="table table-striped" id="tbl-mahasiswa-data">
	      <thead>
	        <tr>
	          <th>ID</th>
	          <th>Judul Buku</th>
	          <th>Pengarang</th>
	          <th>Tahun Terbit</th>
	          <th>Tanggal Input</th>
	          <th>Tanggal Edit</th>
	          <th>Aksi</th>
	        </tr>
	      </thead>
	      <tbody></tbody>
	    </table>
  </div>
</div>
<?= $this->endSection() ?>