<?= $this->extend('template/template') ?>
<?= $this->section('main') ?>

<div class="col col-md-12">
    <?php $session = \Config\Services::session();; if (!empty($session->getFlashdata('message'))) : ?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <?= $session->getFlashdata("message").'</br>' ?>
        </div>
    <?php endif; ?>
</div>
<div class="col col-md-12">
    <?php $session = \Config\Services::session();; if (!empty($session->getFlashdata('error'))) : ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <?php foreach ($session->getFlashdata("error") as $key => $value): ?>
                    <?= $value.'</br>' ?>
                <?php endforeach ?>
            </div>
        <?php endif; ?>
</div>
<div class="card border-success mb-3">
  <div class="card-header">
    Tambah Data Buku
  </div>
    <?= form_open_multipart(base_url('/create_buku')); ?>
    <div class="card-body">
        <div class="mb-3">
          <label class="form-label">Judul Buku</label>
          <input type="text" name="judul_buku" class="form-control"  placeholder="Masukkan Judul Buku" value="<?= old('judul_buku') ?>">
        </div>
        <div class="mb-3">
          <label class="form-label">Pengarang</label>
          <input type="text" name="pengarang" class="form-control" placeholder="Masukan Nama Pengarang">
        </div>
        <div class="mb-3">
          <label class="form-label">Tahun Terbit</label>
          <input type="text" name="tahun_terbit" class="form-control" placeholder="Tahun Terbit">
        </div>
    </div>
    <div class="card-footer">
      <div class="d-grid gap-2">
        <input type="submit" name="simpan" class="btn btn-sm btn-success" value="Simpan" >
          <button class="btn btn-sm btn-warning" type="reset">Batal</button>
        </div>
    </div>
    <?= form_close() ?>
</div>
<?= $this->endSection() ?>