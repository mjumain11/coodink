<?= $this->extend('template/template') ?>
<?= $this->section('main') ?>

<?php
    $encryption = new \Daycry\Encryption\Encryption();
?>

<div class="card border-success mb-3">
  <div class="card-header">
    Tambah Data Buku
  </div>
    <?= form_open_multipart(base_url('/update_buku/'.$encryption->encrypt($data['id']))); ?>
    <div class="card-body">
        <div class="mb-3">
          <label class="form-label">Judul Buku</label>
          <input type="text" name="judul_buku" class="form-control"  placeholder="Masukkan Judul Buku" value="<?= $data['judul_buku'] ?>">
        </div>
        <div class="mb-3">
          <label class="form-label">Pengarang</label>
          <input type="text" name="pengarang" class="form-control" placeholder="Masukan Nama Pengarang" value="<?= $data['pengarang'] ?>">
        </div>
        <div class="mb-3">
          <label class="form-label">Tahun Terbit</label>
          <input type="text" name="tahun_terbit" class="form-control" placeholder="Tahun Terbit" value="<?= $data['tahun_terbit'] ?>">
        </div>
    </div>
    <div class="card-footer">
      <div class="d-grid gap-2">
        <input type="submit" name="simpan" class="btn btn-sm btn-success" value="Simpan" >
          <button class="btn btn-sm btn-warning" type="reset">Batal</button>
        </div>
    </div>
    <?= form_close() ?>
</div>
<?= $this->endSection() ?>