<?= $this->extend('template/template') ?>
<?= $this->section('main') ?>
<?php
    $encryption = new \Daycry\Encryption\Encryption();
?>

<div class="card border-success mb-3">
  <div class="card-header">
    Tambah Data Buku
  </div>
    <div class="card-body">
        <div class="mb-3">
          <label class="form-label">Judul Buku</label>
          <input type="text" disabled name="judul_buku" class="form-control"  placeholder="Masukkan Judul Buku" value="<?= $data['judul_buku'] ?>">
        </div>
        <div class="mb-3">
          <label class="form-label">Pengarang</label>
          <input type="text" disabled name="pengarang" class="form-control" placeholder="Masukan Nama Pengarang" value="<?= $data['pengarang'] ?>">
        </div>
        <div class="mb-3">
          <label class="form-label">Tahun Terbit</label>
          <input type="text" disabled name="tahun_terbit" class="form-control" placeholder="Tahun Terbit" value="<?= $data['tahun_terbit'] ?>">
        </div>
    </div>
    <div class="card-footer">
        <div class="d-grid gap-2">
            <a class="btn btn-sm btn-block btn-danger" onClick="return confirm('Apakah Anda benar-benar mau menghapusnya?')" href="<?= base_url('delete_buku').'/'.$data['id'] ?>">Delete</a>
            <a class="btn btn-sm btn-block btn-success" href="<?= base_url('edit_buku').'/'.$encryption->encrypt($data['id']) ?>">Edit</a>
        </div>
    </div>
</div>
<?= $this->endSection() ?>