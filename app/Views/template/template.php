<!DOCTYPE html>
<html>
<head>
	<title><?= $title ?></title>
</head>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css"/>

<body class="container">
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container-fluid">
    <a class="navbar-brand" href="<?= base_url('login') ?>">
      <img src="https://getbootstrap.com/docs/5.0/assets/brand/bootstrap-logo.svg" alt="" width="30" height="24">
    </a>
    <a class="navbar-brand" href="<?= base_url('login') ?>">APP</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="<?= route_to('buku') ?>">Buku</a>
        </li>
      </ul>
      <div class="btn-group" role="group" aria-label="Basic example">
        <a href="<?= route_to('logout') ?>" class="btn btn-sm btn-outline-secondary">Signout</a>
      </div>
    </div>
  </div>
</nav>

<div class="container">
  <br>
  <?= $this->renderSection('main') ?>
</div>

</body>
<link rel="stylesheet" type="text/css" href="">
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
 
 
  <script>
    var site_url = "<?php echo site_url(); ?>";
    $(document).ready( function () {
        $('#tbl-mahasiswa-data').DataTable({
          lengthMenu: [[ 10, 30, -1], [ 10, 30, "All"]],
          bProcessing: true,
          serverSide: true,
          scrollY: "400px",
          scrollCollapse: true,
          ajax: {
            url: site_url + "/list_buku",
            type: "post",
            data: {
            }
          },
          columns: [
            { data: "id" },
            { data: "judul_buku" },
            { data: "pengarang" },
            { data: "tahun_terbit" },
            { data: "created_at" },
            { data: "updated_at" },
            { data: null,width:"10%",

                        render: function ( data, type, row ) {
                            if ( type === 'display' ) {
                              
                              var btn_group="";
                              var id= data['id'];
                              btn_group = btn_group+ '<a class="btn btn-sm btn-info" href="<?= base_url('view_buku') ?>/'+id+'">Detail Buku</a>';
                              
                              return btn_group;
                            } 
                            
                            return data;  
                        }                       
                      },
          ],
          columnDefs: [
            { orderable: false, targets: [0, 1, 2, 3] }
          ],
          bFilter: true,
        });
    });
  </script>
</html>