<?= $this->extend('template/template') ?>
<?= $this->section('main') ?>
<div class="row">
    <div class="col col-md-12">
    </div>
</div>
 <div class="row">
    <div class="col-3"><img class="img-fluid" src="<?= base_url('/assets/images/'.$data['avatar']) ?>"></div>
    <div class="col-9">
    	<table class="table">
    		<tr>
    			<td>Email</td>
    			<td><?= $data['email'] ?></td>
    		</tr>
    		<tr>
    			<td>Nama</td>
    			<td><?= $data['name'] ?></td>
    		</tr>
    	</table>
    	<div class="btn-group" role="group" aria-label="Basic example">
	        <a href="<?= route_to('edit_member') ?>" class="btn btn-sm btn-outline-secondary">Ubah Profil</a>
	    </div>
    </div>
</div>
<?= $this->endSection() ?>