<?= $this->extend('template/template') ?>
<?= $this->section('main') ?>
<div class="row">
    <div class="col col-md-12">
        <?php $session = \Config\Services::session();; if (!empty($session->getFlashdata('error'))) : ?>
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php foreach ($session->getFlashdata("error") as $key => $value): ?>
                        <?= $value.'</br>' ?>
                    <?php endforeach ?>
                </div>
            <?php endif; ?>
    </div>
</div>
 <div class="row">
    <div class="col-3"><img class="img-fluid" src="ZCCZC"></div>
    <div class="col-9">
        <?= form_open_multipart(base_url('/update_member')); ?>
        <table class="table">
            <tr>
                <td>Email</td>
                <td><input type="text" name="email" value="<?= $data['email'] ?>"></td>
            </tr>
            <tr>
                <td>Nama</td>
                <td><input type="text" name="name" value="<?= $data['name'] ?>"></td>
            </tr>
            <tr>
                <td>Avatar</td>
                <td><input type="file" name="file_upload"></td>
            </tr>
        </table>
        <div class="btn-group" role="group" aria-label="Basic example">
            <input type="submit" value="Simpan" class="btn btn-info" />
        </div>
        <?= form_close() ?>
    </div>
</div>
<?= $this->endSection() ?>