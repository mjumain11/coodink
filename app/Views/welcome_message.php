<!DOCTYPE html>
<html>
<head>
	<title>Selamat Datang</title>
</head>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
<body>
<nav class="navbar navbar-light bg-light">
  <div class="container">
    <a class="navbar-brand" href="#">
      <img src="https://www.svgrepo.com/show/217771/shopping-logo.svg" alt="" width="100" height="50" class="d-inline-block align-text-middle" > Aplikasi Input Data Buku
    </a>
    <div class="btn-group" role="group" aria-label="Basic example">
	  <a href="<?= route_to('register') ?>" class="btn btn-sm btn-outline-secondary">Signup</a>
	  <a href="<?= route_to('login') ?>" class="btn btn-sm btn-outline-primary" >Signin</a>
	</div>
  </div>
</nav>

<div class="container-fluid">
  <div class="row justify-content-md-center">
    <div class="col col-lg-2">
      Selamat Datang di Aplikasi Input Data Buku Sederhana
    </div>
  </div>
</div>

</body>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
</html>