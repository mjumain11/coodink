<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\MemberModel;

class MemberController extends BaseController
{
	protected $member;

	public function __construct()
	{
		// Most services in this controller require
		// the session to be started - so fire it up!
		// $this->session = service('session');
		$this->member = new MemberModel();

		$this->config = config('Auth');
		$this->auth = service('authentication');
        helper('form');
		
	}
    public function index()
    {
        //
        if (!$this->auth->check()) {
			return redirect()->to('login');
		}

        $data = array(
	        	'title' 	=> 'Dashbord Member' , 
	        	'id' 		=> $this->auth->id(),
	        	'data'		=> $this->member->find($this->auth->id()),
        	 );

        echo view('member/dashbord', $data);
        
    }
    public function edit()
    {
        //
        
        if (!$this->auth->check()) {
			return redirect()->to('login');
		}
		
        $data = array(
	        	'title' 	=> 'Ubah Profil' ,
	        	'data'		=> $this->member->find($this->auth->id()),
        	 );

        echo view('member/member_edit', $data);
        
    }

    public function update()
    {
        if (!$this->auth->check()) {
            return redirect()->to('login');
        }
        
    	$validation =  \Config\Services::validation();
    	$validation->setRules([
                'name' => 'required',
                'email' => 'required|min_length[10]',
                'file_upload' => 'uploaded[file_upload]|mime_in[file_upload,image/jpg,image/jpeg,image/gif,image/png]|max_size[file_upload,100]',
        ]);


    	if ($validation->withRequest($this->request)->run()=== false) {
            $session = \Config\Services::session();
            $session->setFlashdata('error', $validation->getErrors());
            return redirect()->to('/edit_member');
        }
        else
        {
            $upload = $this->request->getFile('file_upload');
            $upload->move(WRITEPATH . '../public/assets/images/');
 
            $this->member->update($this->auth->id(), [
            'name'      => $this->request->getVar('name'),
            'avatar'    => $upload->getName()
            // 'username' => $this->request->getVar('username'),
        ]);
        return redirect()->to('/member');
    	}
    }
}
