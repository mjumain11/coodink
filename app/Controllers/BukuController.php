<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\BukuModel;
class BukuController extends BaseController
{
	public $db;
    public $buku;
    public $encryption;

	public function __construct()
	{
		// Most services in this controller require
		// the session to be started - so fire it up!
		// $this->session = service('session');
		$this->config = config('Auth');
		$this->auth = service('authentication');
		$this->db = \Config\Database::connect();
        $this->session = \Config\Services::session();
        $this->validation =  \Config\Services::validation();
        $this->encryption = new \Daycry\Encryption\Encryption();
        $this->buku = new BukuModel();
        helper('form');
	}
    public function index()
    {
        //
        if ($this->auth->check())
		{
            $data = array('title' =>'Data Buku' , );
			return view('buku/buku_list', $data);
		}
		else
		{
			echo "Anda Belum Masuk";
		}

    }
    public function ajaxLoadData()
    {
        $params['draw'] = $_REQUEST['draw'];
        $start = $_REQUEST['start'];
        $length = $_REQUEST['length'];
        $search_value = $_REQUEST['search']['value'];
 
        if(!empty($search_value)){
            $total_count = $this->db->query("SELECT * from buku WHERE id like '%".$search_value."%' OR judul_buku like '%".$search_value."%' OR pengarang like '%".$search_value."%'")->getResult();
 
            $data = $this->db->query("SELECT * from buku WHERE id like '%".$search_value."%' OR judul_buku like '%".$search_value."%' OR pengarang like '%".$search_value."%' limit $start, $length")->getResult();
        }
        else
        {
            $total_count = $this->db->query("SELECT * from buku")->getResult();
            $data = $this->db->query("SELECT * from buku limit $start, $length")->getResult();            
        }

        $json_data = array(
            "draw" => intval($params['draw']),
            "recordsTotal" => count($total_count),
            "recordsFiltered" => count($total_count),
            "data" => $data   // total data array
        );
        echo json_encode($json_data);
    }
    public function add_buku()
    {
        //
        if ($this->auth->check())
        {
            $data = array('title' =>'Tambah Buku' , );
            echo view('buku/buku_add', $data);
        }
        else
        {
            echo "Anda Belum Masuk";
        }
    }
    public function create_buku()
    {
        //
        if ($this->auth->check())
        {
            $input = $this->validate([
                'judul_buku'    => 'required|min_length[3]',
                'pengarang'     => 'required',
                'tahun_terbit'  => 'required|numeric|max_length[5]'
            ]);
            

            if ($this->validation->withRequest($this->request)->run()=== false) {
                
                $this->session->setFlashdata('error', $this->validation->getErrors());
                return redirect()->to('/add_buku');
            }
            else
            {
                $data = array(
                    'judul_buku'        => $this->request->getVar('judul_buku'),
                    'pengarang'    => $this->request->getVar('pengarang'),
                    'tahun_terbit'      => $this->request->getVar('tahun_terbit'),
                    'created_at'        => date('Y:m:d h:i:s'),);
     
                $this->buku->save($data);

                $this->session->setFlashdata('message', "Data Berhasil Disimpan");
                return redirect()->to('/add_buku');    
            }
        }
        else
        {
            echo "Anda Belum Masuk";
        }

    }
    public function view_buku($id)
    {
        if (!$this->auth->check()) {
            return redirect()->to('login');
        }
        
        $data = array(
                'title'     => 'Detail Buku' ,
                'data'      => $this->buku->find($id),
             );

        echo view('buku/buku_view', $data);
    }
    public function delete_buku($id)
    {
        if (!$this->auth->check()) {
            return redirect()->to('login');
        }
        else
        {
            $this->buku->delete($id);
            $this->session->setFlashdata('message', "Data Berhasil Dihapus");
            return redirect()->to('/buku');
        } 
        
    }
    public function edit_buku($id)
    {
        if (!$this->auth->check()) {
            return redirect()->to('login');
        }
        
        $data = array(
                'title'     => 'Edit Buku' ,
                'data'      => $this->buku->find($this->encryption->decrypt($id)),
             );

        echo view('buku/buku_edit', $data);
    }
    public function update_buku($id)
    {
        if (!$this->auth->check()) {
            return redirect()->to('login');
        }
        
        $data = array(
            'judul_buku'        => $this->request->getVar('judul_buku'),
            'pengarang'         => $this->request->getVar('pengarang'),
            'tahun_terbit'      => $this->request->getVar('tahun_terbit'),
            'updated_at'        => date('Y:m:d h:i:s'),);

        $this->buku->update($id,$data);
        $this->session->setFlashdata('message', "Data Berhasil Diubah");
        return redirect()->to('/buku');
    }
}
